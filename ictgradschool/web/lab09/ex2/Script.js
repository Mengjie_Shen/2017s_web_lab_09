/**
 * Created by mshe666 on 7/12/2017.
 */


var articleList = [];
var startIndex = 0;
var authorList = [];
$(function () {

    $('#loadArticleButton').click(function () {
        getList();
        getNameList();
        loadArticle();
/*        var endIndex = (startIndex + 3 <= articleList.length) ? startIndex + 3 : articleList.length;

        for ( ; startIndex < endIndex; startIndex++) {

            var article = articleList[startIndex];
            var article_id = article.id;
            var article_title = article.title;
            var article_author_id = article.author_id;
            var article_content = article.content;

            var panel_title = $('<div class="panel-heading"></div>');
            panel_title.text(article_title);//create panel title

            // console.log(article_id);
            var panel_p = $('<p></p>');
            panel_p.text(article_content);//create panel content
            // panel_p.id = "p" + article_id;
            panel_p.attr('id', "p" + article_id);
            // console.log(panel_p);

            var panel_body = $('<div class="panel-body"></div>');
            panel_body.append(panel_p);//add panel content to panel body

            var panel_foot = $('<div class="text-center bg-info fule-btn">Show full content</div>');
            // panel_foot.id = "show_full_content_" + article_id;
            panel_foot.attr('id', "show_full_content_" + article_id);
            // console.log(panel_foot);

            var panel = $('<div class="panel panel-default pgcertArticle"></div>');

            panel.append(panel_title);
            panel.append(panel_body);
            panel.append(panel_foot);

            $(".pgcertArticleContainer").append(panel);
        }*/
    });

    $('.pgcertArticleContainer').on('click', 'div.fule-btn', function () {
        // console.log(this.id);
        var call_id = this.id.substring(this.id.lastIndexOf('_') + 1);
        ajaxCall(call_id);
        this.remove();
    })

    $('.pgcertArticleContainer').on('click', 'h4.pheads', function () {
        $('.pgcertUserContainer').empty();
        var author_id = this.id.substring(this.id.lastIndexOf('_') + 1);
        // console.log(author_id);
        loadInfo(author_id);
/*        var userPanel_title = $('<div class="panel-heading"></div>');
        userPanel_title.text('User name');

/!*        var userPanelP = $('<div><p><strong>First Name: </strong></p>' +
            '<p><strong>Last Name: </strong></p>' +
            '<p><strong>Gender: </strong></p></div>');
        userPanelP.attr('id', 'userPanelP_id' + author_id);
        console.log(typeof userPanelP.children());
        userPanelP.children()[0].attr('id', 'fname_id' + author_id);
        userPanelP.children()[1].attr('id', 'lname_id' + author_id);
        userPanelP.children()[2].attr('id', 'gender_id' + author_id);*!/

        var fnameP = $('<p><strong>First Name: </strong></p>');
        fnameP.attr('id', 'fname_' + author_id);
        var lnameP = $('<p><strong>Last Name: </strong></p>');
        lnameP.attr('id', 'lname_' + author_id);
        var genderP = $('<p><strong>Gender: </strong></p>');
        genderP.attr('id', 'genger_' + author_id);
        console.log('11111');

        var userPanel_body = $('<div class="panel-body"></div>');
        userPanel_body.append(fnameP);
        userPanel_body.append(lnameP);
        userPanel_body.append(genderP);
        console.log(userPanel_body);

        var userPanel = $('<div class="panel panel-default userPanel"></div>');
        userPanel.append(userPanel_title);
        userPanel.append(userPanel_body);

        ajaxCallAuthor(author_id);*/
    })

})

function loadArticle() {
    (startIndex === 0) ? $(".pgcertArticleContainer").empty() : "";
    var endIndex = (startIndex + 3 <= articleList.length) ? startIndex + 3 : articleList.length;

    for ( ; startIndex < endIndex; startIndex++) {

        var article = articleList[startIndex];
        var article_id = article.id;
        var article_title = article.title;
        var article_author_id = article.author_id;
        var author_name = getName(article_author_id);
        var article_content = article.content;

        var panel_title = $('<div class="panel-heading"></div>');
        panel_title.text(article_title);//create panel title

        // console.log(article_id);

        var panel_phead = $('<h4 class="pheads"></h4>');
        panel_phead.text(author_name);
        panel_phead.attr('id', "phead_" + article_id);
        // console.log(author_name);
        // console.log(panel_phead);

        var panel_p = $('<p></p>');
        panel_p.text(article_content);//create panel content
        // panel_p.id = "p" + article_id;
        panel_p.attr('id', "p" + article_id);
        // console.log(panel_p);

        var panel_body = $('<div class="panel-body"></div>');
        panel_body.append(panel_phead);
        panel_body.append(panel_p);//add panel content to panel body

        var panel_foot = $('<div class="text-center bg-info fule-btn">Show full content</div>');
        // panel_foot.id = "show_full_content_" + article_id;
        panel_foot.attr('id', "show_full_content_" + article_id);
        // console.log(panel_foot);

        var panel = $('<div class="panel panel-default pgcertArticle"></div>');
        // console.log(panel);

        panel.append(panel_title);
        panel.append(panel_body);
        panel.append(panel_foot);

        $(".pgcertArticleContainer").append(panel);
    }

    (startIndex === articleList.length) ? $('#loadArticleButton').attr('style', 'background-color: red') : "";

    /*    if (startIndex === articleList.length) {
     $('#loadArticleButton').removeClass('bg-info');
     $('#loadArticleButton').addClass('bg-danger');
     }*/

}

function loadInfo(author_id) {

    var userPanel_title = $('<div class="panel-heading"></div>');
    userPanel_title.text('User name');

    /*        var userPanelP = $('<div><p><strong>First Name: </strong></p>' +
     '<p><strong>Last Name: </strong></p>' +
     '<p><strong>Gender: </strong></p></div>');
     userPanelP.attr('id', 'userPanelP_id' + author_id);
     console.log(typeof userPanelP.children());
     userPanelP.children()[0].attr('id', 'fname_id' + author_id);
     userPanelP.children()[1].attr('id', 'lname_id' + author_id);
     userPanelP.children()[2].attr('id', 'gender_id' + author_id);*/

    var fnameP = $('<p></p>');
    fnameP.append('<strong>First name: </strong>');
    fnameP.attr('id', 'fname_' + author_id);

    var lnameP = $('<p></p>');
    lnameP.append('<strong>Last name: </strong>');
    lnameP.attr('id', 'lname_' + author_id);

    var genderP = $('<p></p>');
    genderP.append('<strong>Gender: </strong>');
    genderP.attr('id', 'gender_' + author_id);

    var likesP = $('<p></p>');
    likesP.append('<strong>Likes: </strong>');
    likesP.attr('id', 'likes_' + author_id);

    var likeList = $('<ul></ul>');
    likeList.attr('id', 'likelist_' + author_id);
    likesP.append(likeList);

    var userPanel_body = $('<div class="panel-body"></div>');
    userPanel_body.attr('id', 'userbody_' + author_id);
    userPanel_body.append(fnameP, lnameP, genderP, likesP);

    var userPanel = $('<div class="panel panel-default userPanel"></div>');
    userPanel.append(userPanel_title);
    userPanel.append(userPanel_body);

    $('.pgcertUserContainer').append(userPanel);

    ajaxCallAuthor(author_id);
    ajaxCallLikes(author_id);
}

function getList() {
    var myRequest = new XMLHttpRequest();
    myRequest.open('GET', 'https://sporadic.nz/AJAXEndpoint/Articles', false);
    myRequest.onreadystatechange = function () {
        if (myRequest.readyState === 4 && myRequest.status === 200) {
            // console.log(myRequest.responseText);
            articleList = JSON.parse(myRequest.responseText);
        }
    };
    myRequest.send();

}

function getNameList() {
    var myRequest = new XMLHttpRequest();
    myRequest.open('GET', 'https://sporadic.nz/AJAXEndpoint/Users', false);
    myRequest.onreadystatechange = function () {
        if (myRequest.readyState === 4 && myRequest.status === 200) {
            // console.log(myRequest.responseText);
            authorList = JSON.parse(myRequest.responseText);
        }
    };
    myRequest.send();

}

function getName(author_id) {
    for (var i = 0; i < authorList.length; i++) {
        if (authorList[i].id === author_id) {
            // console.log(authorList[i].first_name);
            return authorList[i].first_name;
        }
    }
}


function ajaxCall(num) {
    console.log(num);
    $.ajax({
        url: "https://sporadic.nz/AJAXEndpoint/Articles",
        type: "GET",
        data: {article: num},
        success: function (msg) {
            // console.log(msg.content);
            $('#p' + num).text(msg.content);
        }

    });
}

function ajaxCallAuthor(num) {
    $.ajax({
        url: "https://sporadic.nz/AJAXEndpoint/Users",
        type: "GET",
        data: {id: num},
        success: function (msg) {
            // console.log(msg);
            var fname_span = $('<span></span>');
            var lname_span = $('<span></span>');
            var gender_span = $('<span></span>');
            fname_span.text(msg.first_name);
            lname_span.text(msg.last_name);
            gender_span.text(msg.gender);
            $('#fname_' + num).append(fname_span);
            $('#lname_' + num).append(lname_span);
            $('#gender_' + num).append(gender_span);
        }

    });
}

function ajaxCallLikes(num) {
    console.log(num);
    $.ajax({
        url: "https://sporadic.nz/AJAXEndpoint/Likes",
        type: "GET",
        data: {user: num},
        success: function (msg) {
            for (var i = 0; i < msg.length; i++) {
                var li = $('<li></li>');
                for (var j = 0; j < articleList.length; j++) {
                    if (articleList[j].id === msg[i]) {
                        li.text(articleList[j].title);
                        break;
                    }
                }
                $('#likelist_' + num).append(li);

            }
        }

    });
}